// utility to sort blast results, implements a simple insertion sort algo as well as sorting based on avg ranking 

package rank

import (
    "bufio"
    "fmt"
    "os"
    "sort"
)

type rank struct {
    Taxid string
    AvgRank float64
}

type Ranks []rank

type genome struct {
    Taxid string
    Count float64
    Rank  int
}

func (r Ranks) InsertAt(elem rank, pos int) Ranks{
    last := len(r)
    r = append(r, rank{})
    copy(r[pos+1:], r[pos:last])
    r[pos] = elem

    return r
}

func InsertSortRanks(m map[string]float64) Ranks {
    var sorted Ranks
    var inserted bool

    for key, val := range m {
        inserted = false
        tax := rank{Taxid: key, AvgRank: val}
        for i, v := range sorted {
            if tax.AvgRank < v.AvgRank {
                sorted = sorted.InsertAt(tax, i)
                inserted = true
                break
            }
        }
        if !inserted {
            sorted = append(sorted, tax)
        }
    }
    return sorted
}

// Sort interface definition to sort our Ranks struct
func (r Ranks) Len() int           { return len(r) }
func (r Ranks) Less(i, j int) bool { return r[i].AvgRank < r[j].AvgRank }
func (r Ranks) Swap(i, j int)      { r[i], r[j] = r[j], r[i] }

func MapToRanks(m map[string]float64) Ranks {
    var toSort Ranks
    for id, rnk := range m {
        toSort = append(toSort, rank{Taxid: id, AvgRank: rnk})
    }
    return toSort
}

func SortRanks(m map[string]float64) Ranks {
    sorted := MapToRanks(m)
    sort.Sort(sorted)

    return sorted
}

func (r rank) String() string {
    return fmt.Sprintf("%v,%v", r.Taxid, r.AvgRank)
}

func (r Ranks) Pretty(nb int) {
    fmt.Fprintf(os.Stdout, "%v\n","Taxid,Rank")
    if (nb < 0) || (nb > len(r)) {
        nb = len(r)
    }
    for i := 0; i < nb; i++ {
        fmt.Fprintf(os.Stdout, "%v\n", r[i])
    }
}

func (r Ranks) Write(d, o string) error {
    f, err := os.Create(fmt.Sprintf("%v/%v", d, o))
    defer f.Close()
    if err != nil {
        return err
    }

    w := bufio.NewWriter(f)

    for _, rank := range r {
        fmt.Fprintf(w, "%v\n", rank)
    }
    w.Flush()

    return nil
}

type Counts []genome

func (c Counts) InsertAt(elem genome, pos int) Counts{
    last := len(c)
    c = append(c, genome{})
    copy(c[pos+1:], c[pos:last])
    c[pos] = elem

    return c
}

func InsertSort(m map[string]map[string]bool) Counts {
    var sorted Counts
    var inserted bool

    for key, val := range m {
        inserted = false
        tax := genome{Taxid: key, Count: float64(len(val))}
        for i, v := range sorted {
            if tax.Count > v.Count {
                sorted = sorted.InsertAt(tax, i)
                inserted = true
                break
            }
        }
        if !inserted {
            sorted = append(sorted, tax)
        }
    }
    for i, _ := range sorted {
        sorted[i].Rank = i
    }
    return sorted
}

// Sort interface definition to sort our Counts struct
func (c Counts) Len() int           { return len(c) }
func (c Counts) Less(i, j int) bool { return c[i].Count > c[j].Count }
func (c Counts) Swap(i, j int)      { c[i], c[j] = c[j], c[i] }

func MapToCounts(m map[string]map[string]bool) Counts {
    var toSort Counts
    for key, val := range m {
        toSort = append(toSort, genome{Taxid: key, Count: float64(len(val))} )
    }
    return toSort
}

func SortCounts(m map[string]map[string]bool) Counts {
    sorted := MapToCounts(m)

    sort.Sort(sorted)

    for i, _ := range sorted {
        sorted[i].Rank = i
    }
    return sorted
}

// Requires to slices of equal lengths
// The first contains the identifiers for the elements of the second
// Return a Counts struct for the given slices
func SliceToCounts(id []string, ct []float64) Counts {
    var toSort Counts
    for i := 0; i < len(id); i++ {
        toSort = append(toSort, genome{Taxid: id[i], Count: ct[i]})
    }
    return toSort
}

// Simple sorting of abundance table 
// These slices are used to create the Counts type, which will be sorted. 
func SortAbundance(id []string, ct []float64) Counts {
    sorted := SliceToCounts(id, ct)

    sort.Sort(sorted)

    for i, _ := range sorted {
        sorted[i].Rank = i
    }
    return sorted
}
